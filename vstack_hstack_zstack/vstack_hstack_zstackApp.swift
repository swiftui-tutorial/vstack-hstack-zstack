//
//  vstack_hstack_zstackApp.swift
//  vstack_hstack_zstack
//
//  Created by Kitti Jarearnsuk on 11/9/2565 BE.
//

import SwiftUI

@main
struct vstack_hstack_zstackApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
